jQuery(document).ready(function($){

	// activate bootstrap features

	// $('#action-tabs a').click(function (e) {
	// 	e.preventDefault();
	// 	$(this).tab('show');
	// })
	$('.popover-item').popover();
	$('.tooltip-item').tooltip();

	// first tab active by default
	$('#action-tabs a:first').tab("show");

	// PAGES
	var $about = $("#about");
	var $call = $("#call");

	// Nav menu actions

	// Main logo/home page
	$("a#home").click(function(){
		// console.log(this);

		$("#main-nav li").removeClass("active");

		$about.hide();
		$call.show();
	});

	// Details

	// default view
	$about.show();

	// $("a#details").click(function(){
	// 	// console.log(this);
	// 	activate_top_nav_link(this);

	// 	$about.show();
	// 	$call.hide();
	// });

	// // Pricing
	// $("a#pricing").click(function(){
	// 	// console.log(this);
	// 	activate_top_nav_link(this);

	// 	$about.show();
	// 	$call.hide();
	// });

	// // Rates
	// $("a#rates").click(function(){
	// 	// console.log(this);
	// 	activate_top_nav_link(this);

	// 	$about.show();
	// 	$call.hide();
	// });

	// AUTOCOMPLETE

	$("input.autocomplete").keypress(function(event){
		// console.log(event);
		console.log(event.keyCode);
		console.log(event.shiftKey);

	});

	// sample autocomplete data for address book
	var autocomplete_data = {
		'647 270 1155' : 'Victor Stan',
		'555 666 7777' : 'Aaron Vegh',
		'416 905 1234' : 'Violeta Petrescu',
		'123 456 7890' : 'John Smith'
	}
	console.log(autocomplete_data);

	var update_call_button = function(element, active) {
		$element = element;
		// update button
		if (active == true) {
			$element.removeClass("btn-inverse");
			$element.addClass("btn-danger");
			$element.html('<i class="icon-bell icon-white"></i> Hang-up');
		} else {
			$element.addClass("btn-inverse");
			$element.removeClass("btn-danger");
			$element.removeClass("active");
			$element.html('<i class="icon-bell icon-white"></i> Call');
		};
	};

	// CALL TIMER/COUNTER
	var tick;
	var run_timer = false;
	$("#call-timer").hide();
	$("#call-button").on("click", function() {
		// pretent call
		// check if there are recipients
		console.log($("#telephone-select").select2("val"));
		if (_.any($("#telephone-select").select2("val"))) {

			// update ui

			$(this).toggleClass("active");
			$("#call-timer").toggle("fast");

			if ($(this).hasClass("active")) {
				$("#telephone-select").select2("disable");
				update_call_button($(this), true);
			} else {
				$("#telephone-select").select2("enable");
				update_call_button($(this), false);
				if (run_timer) {
					clearInterval(tick);
				}
			}

			// timer

			var start_time = moment();

			var time_ticker = function() {
				var now = moment();
				var time_diff_s = now.diff(start_time, "seconds");
				var time_diff_m = now.diff(start_time, "minutes");
				var time_diff_h = now.diff(start_time, "hours");

				var time_string = (''+time_diff_s).toHHMMSS();

				console.log(time_diff_s);
				// console.log(start_time.fromNow());
				if (now.diff(start_time, "seconds") > 1) {
					// var m = moment(time_diff, "ss");
					// update view only if counter is past one second
					$("#call-timer .display").text(time_string + " - $" + calculate_cost(time_diff_s));
				}
			};

			if (run_timer) {
				clearInterval(tick);
				run_timer = false;
				$("#call-timer .display").text("");
			} else {
				 tick = setInterval(time_ticker, 1000);
				 run_timer = true;
				 $("#call-timer .display").text("00:00:00 - $0.00");
			};

			// call cost calculator
			var COST_PER_MIN = 0.05;
			var calculate_cost = function(time_span) {
				return ((COST_PER_MIN / 60) * time_span).toFixed(2);
			};
		} else {
			// remove toggle button effect
			update_call_button($(this), false);
			// notify user
			alert("Please add recipients to call");
		};
	});

	// MESSAGE BUTTON
	$("#text-message").hide();
	$("#message-button").on("click", function() {
		// toggle message text area
		$("#text-message").toggle("fast");
	});

	// RECIPIENTS IMPUT
	var sample_data = [
		"Aaron Vegh - 555 666 7777",
		"John Smith - 123 456 7890",
		"Victor Stan - 647 270 1155",
		"Violeta Petrescu - 416 905 1234"
	];

	$("#telephone-select").select2({
		placeholder: "Recipients...",
		allowClear: true,
		tags: sample_data,
    tokenSeparators: [","]
	});

	// UTILS

	function activate_top_nav_link(el){
		// console.log($(el).closest("li").siblings());
		$(el).closest("li").siblings().removeClass("active");
		$(el).closest("li").addClass("active");
	};

	// turn a number string or seconds to a hour minute seconds time format display
	String.prototype.toHHMMSS = function () {
		sec_numb    = parseInt(this);
		var hours   = Math.floor(sec_numb / 3600);
		var minutes = Math.floor((sec_numb - (hours * 3600)) / 60);
		var seconds = sec_numb - (hours * 3600) - (minutes * 60);

		if (hours   < 10) {hours   = "0"+hours;}
		if (minutes < 10) {minutes = "0"+minutes;}
		if (seconds < 10) {seconds = "0"+seconds;}
		var time    = hours+':'+minutes+':'+seconds;
		return time;
	}

});
