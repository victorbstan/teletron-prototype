Teletron.me Prototype
=====================

This is a simple demo project, for an idea to create an easy to use web telephone platform.

## Local Server

If you have PHP 5.4+ installed a quick `php -S localhost:8000` from the root of the project should do it.
